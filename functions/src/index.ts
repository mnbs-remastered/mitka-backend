import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
admin.initializeApp();

const usersDB = admin.firestore().collection("users");
const sniffersDB = admin.firestore().collection("sniffers");


const getUserToken = (uid:string):Promise<string> => {
    return usersDB.doc(uid).get().then(snapshot => {
        const user = snapshot.data()!;
        console.log(user);
        return user.token;
    }).catch(err => {
        console.error(err);
        return Promise.reject();
    });
}



//======================================================================================================================
export const onUserDeleteConstellation = functions.firestore
    .document("/users/{userId}")
    .onUpdate((change, context) => {
        const newPacks = change.after.data()!.packs;
        const oldPacks = change.before.data()!.packs;
        if (newPacks < oldPacks) {
            const difference = oldPacks.filter((pack:string) => !newPacks.includes(pack));

            return usersDB.doc(context.params.userId)
                .collection("mitkas")
                .where("packs", "array-contains", difference[0])
                .get()
                .then(snapshots => {
                    snapshots.forEach(snapshot => {
                        const packs = snapshot.data().packs;
                        snapshot.ref.update({
                            packs: packs.filter((pack:string) => !packs.includes(difference[0]))
                        }).catch(err => console.error(err));
                    });
                }).catch(err => console.error(err));
        } else {
            return null;
        }
    });



export const onRealtimeMitkasUpdate = functions.database
    .ref("/mitkas")
    .onUpdate(change => {
        const mitkasAmount = Object.keys(change.after).length;
        const payload = {
            notification: {
                title: "Database pull",
                body: "Updating data..."
            },
            data: {
                title: "Database pull",
                body: "Updating data...",
                count: `${mitkasAmount}`
            }
        };

        return usersDB.get()
            .then(async snapshots => {
                return admin.messaging().sendToDevice(await getUserToken(""), payload)
                    .catch(err => {
                        console.error(err);
                        return Promise.reject();
                    });
        });
    });



export const onUserRegister = functions.auth.user()
    .onCreate(user => {
        return usersDB.doc(user.email!).get().then(doc => {
            if (!doc.exists) {
                return usersDB.doc(user.email!).create({
                    email: user.email,
                    imageUrl: null,
                    settings: {
                        language: null,
                        theme: null
                    },
                    token: null
                }).catch(err => console.error(err));
            } else {
                return null;
            }
        }).catch(err => console.error(err));
    });




export const onLastSeenUpdate = functions.firestore
    .document("users/{userId}/mitkas/{mitkaId}")
    .onUpdate(async (change, context) => {
        const changedDoc = change.after.data()!;
        const oldDoc = change.before.data()!;
        if (changedDoc.lastSeen.seconds !== oldDoc.lastSeen.seconds) {
            const lastSeen = changedDoc.lastSeen.toDate();
            const payload = {
                notification: {
                    title: "We found your Mitka!",
                    body: `Last seen at ${lastSeen.toTimeString()}`
                },
                data: {
                    lastSeen: lastSeen.toString(),
                    latitude: changedDoc.location.latitude.toString(),
                    longitude: changedDoc.location.latitude.toString()
                }
            };
            console.log(payload);
            const token = await getUserToken(context.params.userId);
            return admin.messaging().sendToDevice(token, payload)
                .catch(err => {
                    console.error(err);
                    return Promise.reject();
                });
        } else {
            console.log("lastSeen is the same");
            return Promise.resolve();
        }
    });



export const onSnifferRequest = functions.https
    .onRequest((request, response) => {
        console.log("Request from sniffer");
        console.log(request.body);
        const snifferRef = sniffersDB.doc(request.body.macAddress);
        snifferRef.update(request.body).then(() => {
            response.status(200).send("Updated");
        }).catch(() => {
            console.log("Creating new document for sniffer");
            snifferRef.create(request.body).then(() => {
                response.status(200).send("Created");
            }).catch(err => {
                console.error(err);
                response.status(500).send("Fail");
            });
        })
    });


export const onPackReminderCreate = functions.firestore
    .document("/users/{userId}/packs/{packId}/reminders/{reminderId}")
    .onCreate((snapshot, context) => {
        return usersDB.doc(context.params.userId)
            .collection("packs")
            .doc(context.params.packId)
            .collection("reminders")
            .doc(context.params.reminderId)
            .update({
                id: context.params.reminderId
            }).catch(err => {
                console.error(err);
                return Promise.reject();
            });
    });


export const onMitkaReminderCreate = functions.firestore
    .document("/users/{userId}/mitkas/{mitkaId}/reminders/{reminderId}")
    .onCreate((snapshot, context) => {
        return usersDB.doc(context.params.userId)
            .collection("mitkas")
            .doc(context.params.mitkaId)
            .collection("reminders")
            .doc(context.params.reminderId)
            .update({
                id: context.params.reminderId
            }).catch(err => {
                console.error(err);
                return Promise.reject();
            });
    });